<?php
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles() {
  wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css');
  wp_enqueue_script('jquery-script','https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js');
  wp_enqueue_script('google-maps-api','https://maps.googleapis.com/maps/api/js?key=AIzaSyD8l5talzoQM48PlQMzaC0f5hYGQj0_0-c');
  wp_enqueue_script('custom_functions', get_stylesheet_directory_uri().'/js/functions.js');
}

function mailtrap($phpmailer) {
  $phpmailer->isSMTP();
  $phpmailer->Host = 'smtp.mailtrap.io';
  $phpmailer->SMTPAuth = true;
  $phpmailer->Port = 2525;
  $phpmailer->Username = '3d5ce895877ffb';
  $phpmailer->Password = '4cb8984a107146';
}
add_action('phpmailer_init', 'mailtrap');



function mi_mapa_function($atts = []){
  $atributos = shortcode_atts(array(
    'latlng' => '0,0',
  ),$atts);
  d($atributos);
  ?>
    <script>
     <?php $uniqueid =uniqid();?>
      var map_<?php echo $uniqueid; ?>;
      var coords_<?php echo $uniqueid; ?> = '<?php echo $atributos['latlng'];?>';
      var marcadores = new Array();
      var arrayCocinas = new Array();

      jQuery(document).ready(function() {
        checkGoogleMapsApi(map_<?php echo $uniqueid; ?>,'map_<?php echo $uniqueid; ?>', coords_<?php echo $uniqueid; ?>);
      })
    </script>

  <?php
  return '<div id="map_'.$uniqueid.'" class="map_class"></div>';
}
add_shortcode('mi_mapa','mi_mapa_function');



// Add Shortcode
function mostrar_noticias_function( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'categorias' => '',
		),
		$atts
	);

  // WP_Query arguments
  $args = array(
  	'post_type'              => array( 'post' ),
  	'post_status'            => array( 'publish' ),
  	'order'                  => 'DESC',
  	'orderby'                => 'date',
     'category_name' => 'noticias-internacionales'
  );

  // The Query
  $query = new WP_Query( $args );

  $result = "";

  // The Loop
  if ( $query->have_posts() ) {
    $result.= '<ul>';
  	while ( $query->have_posts() ) {
  		$query->the_post();
  		$result.= '<li><a href="'.get_the_permalink().'">' . get_the_title() . '</a></li>';
  	}
  	$result.= '</ul>';
  	/* Restore original Post Data */
  	wp_reset_postdata();
  } else {
  	// no posts found
  }

  // Restore original Post Data
  wp_reset_postdata();
  //d($result);
  return $result;

}
add_shortcode( 'mostrar_noticias', 'mostrar_noticias_function' );


function mostrar_pestanas_function(){
  ?>
  <style>
  body {font-family: Arial;}

  /* Style the tab */
  .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
  }

  /* Style the buttons inside the tab */
  .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
      background-color: #ddd;
  }

  /* Create an active/current tablink class */
  .tab button.active {
      background-color: #ccc;
  }

  /* Style the tab content */
  .tabcontent {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
  }
  </style>
<h2>Tabs</h2>
<p>Click on the buttons inside the tabbed menu:</p>

<div class="tab">
  <?php
  $args = array();

  $categorias = get_categories($args);
  foreach ($categorias as $categoria) {
    echo '<button class="tablinks" onclick="openCity(event, \''.$categoria->slug.'\')">'.$categoria->name.'</button>';
  }

   ?>
</div>

<?php
foreach ($categorias as $categoria) {
  ?>
  <div id="<?php echo $categoria->slug; ?>" class="tabcontent">
    <h3><?php echo $categoria->name; ?></h3>
    <p>London is the capital city of England.</p>
  </div>
  <?php
}
?>


<div id="London" class="tabcontent">
  <h3>London</h3>
  <p>London is the capital city of England.</p>
</div>

<div id="Paris" class="tabcontent">
  <h3>Paris</h3>
  <p>Paris is the capital of France.</p>
</div>

<div id="Tokyo" class="tabcontent">
  <h3>Tokyo</h3>
  <p>Tokyo is the capital of Japan.</p>
</div>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>


  <?php
}
add_shortcode('mostrar_pestanas', 'mostrar_pestanas_function');
