<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

 // Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'J1 hC=tI>!s*#P1$9{TRv:5&(B|j<UW Su*@O-^x2xgA m$ym?S{9wD%=qQaI*HC');
define('SECURE_AUTH_KEY', 'T(ZVBullpc@@<ZwJnk8EAhysUbM!noeAux3:z>SKyj4.YyPbhA=A{%iLKdd5n$9(');
define('LOGGED_IN_KEY', 'Zy>LU&Vnbf-dBpi+Dt)iWkf^N_}=_Z:lOiK,#C9xK@ETP+Fe.![>y>>Vb0s?4G^u');
define('NONCE_KEY', 'wFn4`Hp^+j(bH~U,@D@ ?nTC d5{*u$z.YX4*d1DC]~E|%lu,68jHKrz]:dY7<Gv');
define('AUTH_SALT', 'yG(mAQMCzS%sah#02cl^XXeVYDG>~_q37ykFi$rg!FwFl#z9E-(n%#FCn*g^qzyo');
define('SECURE_AUTH_SALT', 'gf&_-Ipu%?]>@EIbOaOzNo}+4DL.dgN)qjX3VK]bm+8Y-Xk*-OY9<0|KLba80WTI');
define('LOGGED_IN_SALT', 'nnPd@rY4xv}-_8]ISG&Gy,iT.Tl7iNJ.crQ+owF>kEeJJc3EGk24d&%D0L/9=`5q');
define('NONCE_SALT', '6^airRs5@F$zLwQ,(,pj#H#gkP&d*J(}npj GBER3u{ASA51~* &($*uTz?6h?~j');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
